require 'colored'

workspace = "TAMaps.xcworkspace"
scheme = "TAMaps"
test_scheme = "NMAKitTests"
kit_scheme = "NMAKit"

namespace :ci do
	desc "Runs the unit tests"
	task :test    do
		dependencies()
		system "pod install"

		fail unless system "xcrun xcodebuild test -workspace #{workspace} \
											 -scheme #{test_scheme} \
											 | xcpretty -c" 
		
		system "chmod a+x ./Pods/XcodeCoverage/run_code_coverage_post.sh && ./Pods/XcodeCoverage/run_code_coverage_post.sh"		                                     

	end

	desc "Builds the application"
	task :build  do
		#dependencies()

		system "pod install"

		puts "BUILDING IOS APP"
		configuration = "Debug"
		derived_data_path = "build"

		fail unless puts "xcrun xcodebuild -workspace #{workspace} \
		                                     -scheme #{kit_scheme} \
		                                     -configuration #{configuration} \
		                                     -sdk iphonesimulator \
		                                     -derivedDataPath #{derived_data_path} \
		                                     clean build | xcpretty -c"


	end


	def command?(command)
	       system("which #{ command} > /dev/null 2>&1")
	end

	def dependencies()

		fail "Please install dependencies using 'bundle install'".red unless command?("pod")
		fail "Please install dependencies using 'bundle install'".red unless command?("xcpretty")
	end
end





