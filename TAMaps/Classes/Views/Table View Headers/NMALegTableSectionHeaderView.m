//
// Created by Jan Chaloupecky on 15/02/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMALegTableSectionHeaderView.h"
#import "NMAStyleKit.h"
#import "NMAStyleKit+TAFont.h"


@implementation NMALegTableSectionHeaderView {

}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self customizeStyle];
    }

    return self;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customizeStyle];
}

-(void) customizeStyle
{
    self.startTitleLabel.font = [NMAStyleKit boldFontH5];
    self.endTitleLabel.font = [NMAStyleKit boldFontH5];

    self.startValueLabel.font = [NMAStyleKit fontH4];
    self.endValueLabel.font = [NMAStyleKit fontH4];

    self.lengthLabel.font = [NMAStyleKit boldFontH5];
    self.travelTimeLabel.font = [NMAStyleKit boldFontH5];

}

@end