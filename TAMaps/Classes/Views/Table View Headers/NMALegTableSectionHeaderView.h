//
// Created by Jan Chaloupecky on 15/02/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NMALegTableSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) IBOutlet UILabel *startTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *startValueLabel;

@property (nonatomic, weak) IBOutlet UILabel *endTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *endValueLabel;

@property (nonatomic, weak) IBOutlet UILabel *lengthLabel;
@property (nonatomic, weak) IBOutlet UILabel *travelTimeLabel;


@end