//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIkit/UIKit.h>


@interface NMAManeuverTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *instructionLabel;
@end