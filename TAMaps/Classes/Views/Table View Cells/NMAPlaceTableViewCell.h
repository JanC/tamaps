//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NMAPlaceTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *placeTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *placeSubtitleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *placeImageView;

@end