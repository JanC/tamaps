//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAPlaceTableViewCell.h"
#import "NMAStyleKit.h"
#import "NMAStyleKit+TAColor.h"
#import "NMAStyleKit+TAFont.h"


@implementation NMAPlaceTableViewCell {

}

- (UITableViewCell *)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
       [self customizeStyle];
    }

    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customizeStyle];
}

-(void) customizeStyle
{

    self.placeTitleLabel.textColor = [NMAStyleKit textColor];
    self.placeSubtitleLabel.textColor = [NMAStyleKit textLightColor];

    self.placeTitleLabel.font =   [NMAStyleKit fontH4];
    self.placeSubtitleLabel.font =   [NMAStyleKit fontH5];

}


@end