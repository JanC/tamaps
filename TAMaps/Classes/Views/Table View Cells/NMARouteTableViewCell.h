//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NMARouteTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *summaryLabel;
@property (nonatomic, strong) IBOutlet UILabel *distanceLabel;
@property (nonatomic, strong) IBOutlet UILabel *travelTimeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *transportModeImageView;

@end