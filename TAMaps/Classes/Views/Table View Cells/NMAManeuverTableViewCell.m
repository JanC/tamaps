//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAManeuverTableViewCell.h"
#import "NMAStyleKit.h"
#import "NMAStyleKit+TAColor.h"
#import "NMAStyleKit+TAFont.h"


@implementation NMAManeuverTableViewCell {

}

- (UITableViewCell *)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        [self customizeStyle];
    }

    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customizeStyle];
}

-(void) customizeStyle
{
    self.instructionLabel.textColor = [NMAStyleKit textColor];
    self.instructionLabel.font =   [NMAStyleKit fontH4];

}
@end