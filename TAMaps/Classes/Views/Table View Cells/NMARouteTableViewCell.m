//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARouteTableViewCell.h"
#import "NMAStyleKit.h"
#import "NMAStyleKit+TAColor.h"
#import "NMAStyleKit+TAFont.h"


@implementation NMARouteTableViewCell {

}

- (UITableViewCell *)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
       [self customizeStyle];
    }

    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customizeStyle];
}

-(void) customizeStyle
{

    self.travelTimeLabel.textColor = [NMAStyleKit textColor];
    self.distanceLabel.textColor = [NMAStyleKit textLightColor];

    self.travelTimeLabel.font =   [NMAStyleKit fontH2];
    self.distanceLabel.font =   [NMAStyleKit fontH5];

}


@end