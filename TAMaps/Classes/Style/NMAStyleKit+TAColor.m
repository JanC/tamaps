//
// Created by Jan Chaloupecky on 15/02/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <HexColors/HexColor.h>
#import "NMAStyleKit+TAColor.h"


@implementation NMAStyleKit (TAColor)

+ (UIColor*)navigationBarTintColor
{
    return [UIColor colorWithHexString:@"#12B1E2" alpha:1.0];
}

+ (UIColor*)tintColor
{
    return [UIColor colorWithHexString:@"#12B1E2" alpha:1.0];
}

+ (UIColor *)textColor
{
    return [UIColor blackColor];
}

+ (UIColor *)textLightColor
{

    return [UIColor colorWithHexString:@"#000000" alpha:0.5];
}

+ (UIColor *)textInversedColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)textInversedLightColor
{
    return [UIColor colorWithHexString:@"#FFFFFF" alpha:0.5];
}



@end