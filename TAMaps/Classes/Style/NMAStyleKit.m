//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAStyleKit.h"
#import "NMAStyleKit+TAFont.h"
#import "NMAStyleKit+TAColor.h"


@implementation NMAStyleKit

+(NSDictionary *) attributesForEmptyTableTitle
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    NSDictionary *attributes = @{NSFontAttributeName: [NMAStyleKit boldFontH3],
            NSForegroundColorAttributeName: [NMAStyleKit textLightColor],
            NSParagraphStyleAttributeName: paragraphStyle};
    return attributes;

}
+(NSDictionary *)attributesForEmptyTableSubtitle
{

    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
            NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
            NSParagraphStyleAttributeName: paragraphStyle};
    return attributes;

}




@end
