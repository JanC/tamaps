//
// Created by Jan on 02/08/14.
// Copyright (c) 2014 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMAStyleKit.h"

@interface NMAStyleKit (TAFont)


// 35
+ (UIFont *)fontH1;

// Neue Thin 25
+ (UIFont *)fontH2;

// Neue Thin 19
+ (UIFont *)fontH3;

+ (UIFont *)boldFontH3;
+ (UIFont *)boldFontH5;

// Neue Thin 15
+ (UIFont *)fontH4;

// Neue Light 12
+ (UIFont *)fontH5;

// Neue Light 9
+ (UIFont *)fontH6;

+ (NSDictionary *)attributesForEmptyTableTitle;

+ (NSDictionary *)attributesForEmptyTableSubtitle;
@end