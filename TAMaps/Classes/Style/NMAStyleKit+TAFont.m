//
// Created by Jan on 02/08/14.
// Copyright (c) 2014 Tequila Apps. All rights reserved.
//


#import "NMAStyleKit+TAColor.h"

const int TAStyleKitFontSizeBig = 17;
const int TAStyleKitFontSizeSmall = 10;
const int TAStyleKitFontSize = 12;

const int TAStyleKitFontH1 = 35;
const int TAStyleKitFontH2 = 25;
const int TAStyleKitFontH3 = 17;
const int TAStyleKitFontH4 = 15;
const int TAStyleKitFontH5 = 12;
const int TAStyleKitFontH6 = 9;

@implementation NMAStyleKit (TAFont)



+ (UIFont *)defaultFontBig
{
    return [UIFont fontWithName:[self defaultFontName] size:TAStyleKitFontSizeBig];
}

+ (UIFont *)defaultFontSmall
{
    return [UIFont fontWithName:[self defaultFontName] size:TAStyleKitFontSizeSmall];
}

+ (UIFont *)defaultBoldFontSmall
{
    return [UIFont fontWithName:[self defaultBoldFontName] size:TAStyleKitFontSizeSmall];
}

+ (UIFont *)defaultBoldFont
{
    return [UIFont fontWithName:[self defaultBoldFontName] size:TAStyleKitFontSize];
}

+ (NSString *)defaultFontName
{
    return @"NokiaPureText-Regular";
}

+ (NSString *)lightFontName
{
    return @"NokiaPureText-Light";
}

+ (NSString *)defaultBoldFontName
{
    return @"NokiaPureText-Bold";
}


+ (UIFont *)fontH1
{
    return [UIFont fontWithName:[self defaultFontName] size: TAStyleKitFontH1];
}

+ (UIFont *)fontH2
{
    return [UIFont fontWithName:[self defaultFontName] size: TAStyleKitFontH2];
}

+ (UIFont *)fontH3
{
    return [UIFont fontWithName:[self defaultFontName] size: TAStyleKitFontH3];
}

+ (UIFont *)boldFontH3
{
    return [UIFont fontWithName:[self defaultBoldFontName] size:TAStyleKitFontH3];
}

+ (UIFont *)boldFontH5
{
    return [UIFont fontWithName:[self defaultBoldFontName] size:TAStyleKitFontH5];
}

+ (UIFont *)fontH4
{
    return [UIFont fontWithName:[self defaultFontName] size: TAStyleKitFontH4];
}

+ (UIFont *)fontH5
{
    return [UIFont fontWithName:[self lightFontName] size: TAStyleKitFontH5];
}

+ (UIFont *)fontH6
{
    return [UIFont fontWithName:[self lightFontName] size: TAStyleKitFontH6];
}

+(NSDictionary *) attributesForEmptyTableTitle
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    NSDictionary *attributes = @{NSFontAttributeName: [NMAStyleKit boldFontH3],
            NSForegroundColorAttributeName: [NMAStyleKit  textLightColor],
            NSParagraphStyleAttributeName: paragraphStyle};
    return attributes;

}
+(NSDictionary *)attributesForEmptyTableSubtitle
{

    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    NSDictionary *attributes = @{NSFontAttributeName:  [NMAStyleKit fontH3],
            NSForegroundColorAttributeName: [NMAStyleKit  textLightColor],
            NSParagraphStyleAttributeName: paragraphStyle};
    return attributes;

}

#pragma mark - Private




@end