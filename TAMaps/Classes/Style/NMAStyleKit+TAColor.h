//
// Created by Jan Chaloupecky on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMAStyleKit.h"



@interface NMAStyleKit (TAColor)


+ (UIColor*)navigationBarTintColor;
+ (UIColor*)tintColor;

+ (UIColor*)textColor;
+ (UIColor*)textLightColor;

+ (UIColor*)textInversedColor;
+ (UIColor*)textInversedLightColor;



@end