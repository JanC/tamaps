//
// Created by Jan on 12/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIScrollView+EmptyDataSet.h"


@interface NMAEmptyTableHandler : NSObject <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (nonatomic, assign) BOOL loading;
@property (nonatomic, assign) NSError *error;

- (instancetype)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image emptyTableTappedBlock:(void (^)())emptyTableTappedBlock;

+ (instancetype)handlerWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image emptyTableTappedBlock:(void (^)())emptyTableTappedBlock;


@end