//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMASearchResultViewController.h"
#import "NMAPlaceTableViewCell.h"
#import "UIViewController+NMACellConfiguration.h"
#import "UIScrollView+EmptyDataSet.h"
#import "NMAEmptyTableHandler.h"

NSString *const NMASearchResultViewControllerCellId = @"NMASearchResultViewControllerCellId";


@interface NMASearchResultViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>


@property(nonatomic, strong) NMAEmptyTableHandler *emptyTableHandler;
@end

@implementation NMASearchResultViewController {

}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];


    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NMAPlaceTableViewCell class]) bundle:nil] forCellReuseIdentifier:NMASearchResultViewControllerCellId];

    self.tableView.estimatedRowHeight = 70.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    self.emptyTableHandler = [NMAEmptyTableHandler handlerWithTitle:NSLocalizedString(@"No places found.", nil)
                                                           subtitle:NSLocalizedString(@"Keep on typing", nil)
                                                              image:[UIImage imageNamed:@"search_icon"]
                                              emptyTableTappedBlock:nil] ;
    
    self.tableView.emptyDataSetSource = self.emptyTableHandler;
    self.tableView.emptyDataSetDelegate = self.emptyTableHandler;

    self.tableView.tableFooterView = [UIView new];
    self.searchText = @"";
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.placesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NMASearchResultViewControllerCellId forIndexPath:indexPath];

    [self nma_configurePlaceCell:cell withPlace:self.placesArray[(NSUInteger) indexPath.row]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];


    if ([self.resultDelegate respondsToSelector:@selector(searchResultTableViewController:didSelectPlace:)]) {
        [self.resultDelegate searchResultTableViewController:self didSelectPlace:self.placesArray[(NSUInteger) indexPath.row]];
    }
}


#pragma mark - Acessors

- (void)setPlacesArray:(NSArray *)placesArray
{
    _placesArray = placesArray;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.tableView reloadData];
    }];
}

- (void)setSearchError:(NSError *)searchError
{
    _searchError = searchError;
    self.emptyTableHandler.error = searchError;
}


@end