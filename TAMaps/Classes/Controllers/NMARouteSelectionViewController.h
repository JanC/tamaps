//
// Created by Jan on 12/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>


@import NMAKit;




@interface NMARouteSelectionViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NMARouteManagerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *waypointsPlaces; //NMAPlaces

@end