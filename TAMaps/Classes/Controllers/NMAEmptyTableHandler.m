//
// Created by Jan on 12/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAEmptyTableHandler.h"
#import "NMAStyleKit.h"
#import "NMAStyleKit+TAFont.h"

@interface NMAEmptyTableHandler ()
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *subtitle;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, copy) void (^emptyTableTappedBlock)();

@end

@implementation NMAEmptyTableHandler {

}


- (instancetype)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image emptyTableTappedBlock:(void (^)())emptyTableTappedBlock
{
    self = [super init];
    if (self) {
        self.title = title;
        self.subtitle = subtitle;
        self.image = image;
        self.emptyTableTappedBlock = emptyTableTappedBlock;
    }

    return self;
}

+ (instancetype)handlerWithTitle:(NSString *)title subtitle:(NSString *)subtitle image:(UIImage *)image emptyTableTappedBlock:(void (^)())emptyTableTappedBlock
{
    return [[self alloc] initWithTitle:title subtitle:subtitle image:image emptyTableTappedBlock:emptyTableTappedBlock];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = self.title;
    NSDictionary *attributes = [NMAStyleKit attributesForEmptyTableTitle];
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];

    return attributedTitle;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    if (self.loading) {
        return nil;
    }

    NSString *text = self.error ? NSLocalizedString(@"Oups, something wrong happened", nil) : self.subtitle;
    NSDictionary *attributes = [NMAStyleKit attributesForEmptyTableSubtitle];
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];

    return attributedTitle;
}


- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    if (self.loading || (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))) {
        return nil;
    }

    return self.image;
}

#pragma mark - DZNEmptyDataSetDelegate

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    if (self.emptyTableTappedBlock) {
        self.emptyTableTappedBlock();
    }

}
@end