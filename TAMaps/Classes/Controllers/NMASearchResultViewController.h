//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@import NMAKit;

@class NMASearchResultViewController;

@protocol NMASearchResultViewControllerDelegate <NSObject>

- (void)searchResultTableViewController:(NMASearchResultViewController *)controller didSelectPlace:(NMAPlace *)place;

@end

@interface NMASearchResultViewController : UITableViewController

@property(nonatomic, strong) NSArray *placesArray;
@property(nonatomic, strong) NSError *searchError;
@property(nonatomic, copy) NSString *searchText;

@property(nonatomic, weak) id <NMASearchResultViewControllerDelegate> resultDelegate;
@property(nonatomic, assign) BOOL loading;
@end