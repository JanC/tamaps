//
// Created by Jan on 12/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARouteSelectionViewController.h"
#import "NMARoutingOptions.h"
#import "UIViewController+NMACellConfiguration.h"
#import "NMARouteViewController.h"

typedef NS_ENUM(NSUInteger, NMARoutingTransportCell) {
    NMARoutingTransportCellPedestrian,
    NMARoutingTransportCellCar,
    NMARoutingTransportCellPublicTransport,
    NMARoutingTransportCellCount,
};

NSString *const NMARouteTableViewCellId = @"NMARouteTableViewCellId";
NSString *const NMAShowRouteSegueId = @"NMAShowRouteSegueId";

CGFloat const NNAEstimatedRouteRowHeight = 85.0;


@interface NMARouteSelectionViewController ()

@property(nonatomic, strong) NMARouteManager *routeManager;

@property(nonatomic, strong) NSMutableDictionary *calculatedRoutes; // mapping by NMARoutingTransportCell to a NMARoute

@property(nonatomic, strong) NMARoute *selectedRoute;
@end

@implementation NMARouteSelectionViewController {

}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Route Options", nil);
    self.routeManager.delegate = self;
    self.calculatedRoutes = [NSMutableDictionary dictionary];

    self.tableView.estimatedRowHeight = NNAEstimatedRouteRowHeight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.routeManager = [[NMARouteManager alloc] initWithWaypoints:self.waypointsPlaces];
    self.routeManager.delegate = self;

    [self calculateRoutesWithOptions:[NMARoutingOptions defaultOptions]];
}


#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return NMARoutingTransportCellCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NMARouteTableViewCellId forIndexPath:indexPath];
    NMARoute *route = self.calculatedRoutes[@(indexPath.row)];
    [self nma_configureRouteSummaryCell:cell withRoute:route transportMode:[self transportModeForCell:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedRoute = self.calculatedRoutes[@( indexPath.row )];
    [self performSegueWithIdentifier:NMAShowRouteSegueId sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:NMAShowRouteSegueId]) {
        NMARouteViewController *routeViewController = segue.destinationViewController;
        routeViewController.route = self.selectedRoute;
    }
}


#pragma mark - Actions

- (void)calculateRoutesWithOptions:(NMARoutingOptions *)options
{
    [self.routeManager calculateRoutesWithOptions:options];
}

#pragma mark - NMARouteManagerDelegate

- (void)routeManager:(NMARouteManager *)manager calculatedRoute:(NMARoute *)route
{
    // reload given cell
    NMARoutingTransportCell cellIndex = [self cellForTransportMode:route.options.routingTransportMode];
    if (route) {
        self.calculatedRoutes[@(cellIndex)] = route;
    }

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.tableView reloadData];
    }];

}


- (void)routeManager:(NMARouteManager *)manager failedWithError:(NSError *)error forTransportMode:(NMARoutingTransportMode)transportMode
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.tableView reloadData];
    }];
}

#pragma mark - Private

- (NMARoutingTransportCell)cellForTransportMode:(NMARoutingTransportMode)transportMode
{
    switch (transportMode) {

        case NMARoutingTransportModeCar:
            return NMARoutingTransportCellCar;
        case NMARoutingTransportModePedestrian:
            return NMARoutingTransportCellPedestrian;
        case NMARoutingTransportModePublicTransport:
            return NMARoutingTransportCellPublicTransport;
        default:
            return NMARoutingTransportCellPublicTransport;
    }
}

- (NMARoutingTransportMode)transportModeForCell:(NMARoutingTransportCell)cellIndex
{
    switch (cellIndex) {

        case NMARoutingTransportCellCar:
            return NMARoutingTransportModeCar;
        case NMARoutingTransportCellPedestrian:
            return NMARoutingTransportModePedestrian;
        case NMARoutingTransportCellPublicTransport:
            return NMARoutingTransportModePublicTransport;
        default:
            return NMARoutingTransportModeCar;
    }
}


@end