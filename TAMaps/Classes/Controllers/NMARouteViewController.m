//
//  NMARouteViewController.m
//  TAMaps
//
//  Created by Jan Chaloupecky on 07/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARouteViewController.h"
#import "NMARouteClient.h"
#import "TAMapViewController.h"
#import "NMAManeuver.h"
#import "NMAManeuverTableViewCell.h"

#import "NMALeg.h"
#import "NMALegTableSectionHeaderView.h"
#import "NSNumber+NMAFormat.h"


NSString *const TAManeuvreCellId = @"TAManeuvreCellId";
NSString *const NMALegTableSectionHeaderViewId = @"NMALegTableSectionHeaderViewId";
NSString *const TAShowMapSegueId = @"showMapSegue";


const int NMASectionHeaderHeight = 85;

@interface NMARouteViewController ()


@end

@implementation NMARouteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Your Route", nil);

    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    self.tableView.sectionFooterHeight = 0.0f;

    [self.tableView    registerNib:[UINib nibWithNibName:NSStringFromClass([NMALegTableSectionHeaderView class]) bundle:nil]
forHeaderFooterViewReuseIdentifier:NMALegTableSectionHeaderViewId];


}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NMALeg *leg = self.route.legs[(NSUInteger) section];
    return leg.maneuvers.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.route.legs.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NMALeg *leg = self.route.legs[section];
    NMALegTableSectionHeaderView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NMALegTableSectionHeaderViewId];
    sectionHeaderView.startValueLabel.text = leg.startName;
    sectionHeaderView.endValueLabel.text = leg.endName;

    sectionHeaderView.travelTimeLabel.text = [leg.travelTime nma_formatDurationValue];
    sectionHeaderView.lengthLabel.text = [leg.length nma_formatDistanceValue];


    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return NMASectionHeaderHeight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:TAManeuvreCellId forIndexPath:indexPath];

    [self configureManeuvreCell:cell atIndexPath:indexPath];

    return cell;
}


#pragma mark - Actions


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:TAShowMapSegueId]) {
        TAMapViewController *mapViewController = segue.destinationViewController;
        mapViewController.route = self.route;
    }
}


#pragma mark - Cell Configuration

- (void)configureManeuvreCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NMALeg *leg = self.route.legs[(NSUInteger) indexPath.section];

    NMAManeuverTableViewCell *taCell = (NMAManeuverTableViewCell *) cell;
    NMAManeuver *maneuver = leg.maneuvers[(NSUInteger) indexPath.row];
    taCell.instructionLabel.text = maneuver.instruction;
}


@end
