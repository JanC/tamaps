//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+EmptyDataSet.h"




@interface NMAWaypointsViewController : UIViewController

@property(nonatomic, weak) IBOutlet UITableView *tableView;

- (IBAction)editButtonAction:(id)sender;

- (IBAction)routeButtonAction:(id)sender;

@end