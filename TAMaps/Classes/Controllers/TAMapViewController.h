//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@import MapKit;
@import NMAKit;


@interface TAMapViewController : UIViewController

@property(nonatomic, strong) IBOutlet MKMapView *mapView;
@property(nonatomic, strong) NMARoute *route;

@end