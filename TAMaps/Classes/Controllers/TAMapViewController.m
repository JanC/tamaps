//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "TAMapViewController.h"
#import "NMAManeuver.h"
#import "NMARoute.h"

@import CoreLocation;


@interface TAMapViewController () <MKMapViewDelegate>

@property(nonatomic, strong) CLLocationManager *manager;
@property(nonatomic, strong) NSArray *allManeuvres;
@end

@implementation TAMapViewController {

}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.mapView.delegate = self;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [super viewDidAppear:animated];
    self.allManeuvres = [self.route allManeuvres];
    [self.mapView showAnnotations:self.allManeuvres animated:NO];
    [self addRouteOverlay];
    self.manager = [[CLLocationManager alloc] init];
    [self.manager requestWhenInUseAuthorization];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[NMAManeuver class]]) {

        MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
        } else {
            annotationView.annotation = annotation;
        }

        // hmm this could be  better e.g setting the type on the NMAManeur itself

        UIImage *image = [UIImage imageNamed:@"marker_maneuver"];
        if (annotation == [self.allManeuvres firstObject]) {
            image = [UIImage imageNamed:@"icon_maneuver_start"];
        }
        if (annotation == [self.allManeuvres lastObject]) {
            image = [UIImage imageNamed:@"icon_maneuver_finish"];
        }
        annotationView.image = image;
        return annotationView;
    }

    return nil;
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineRenderer *polylineRenderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    polylineRenderer.strokeColor = [UIColor blueColor];
    polylineRenderer.alpha = 0.5;
    polylineRenderer.lineWidth = 4.0;

    return polylineRenderer;
}


#pragma mark - Private

- (void)addRouteOverlay
{
    NSUInteger numberOfSteps = self.allManeuvres.count;

    CLLocationCoordinate2D coordinates[numberOfSteps];
    for (NSInteger index = 0; index < numberOfSteps; index++) {
        NMAManeuver *maneuver = self.allManeuvres[(NSUInteger) index];
        coordinates[index] = maneuver.coordinate;
    }

    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:coordinates count:numberOfSteps];
    [self.mapView addOverlay:polyLine];

}


@end