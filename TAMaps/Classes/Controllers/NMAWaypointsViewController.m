//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAWaypointsViewController.h"
#import "NMASearchResultViewController.h"

#import "UIViewController+NMACellConfiguration.h"
#import "NMAPlaceTableViewCell.h"
#import "NMAEmptyTableHandler.h"
#import "CLLocationManager+NMALocation.h"
#import "NMARouteSelectionViewController.h"

@import NMAKit;


NSString *const NMAWaypointsViewControllerCellId = @"NMAWaypointsViewControllerCellId";

NSString *const NMAShowRouteSelectionSegueId = @"NMAShowRouteSelectionSegueId";

// too many delegates here :(
@interface NMAWaypointsViewController () <UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, NMASearchResultViewControllerDelegate, UISearchBarDelegate, UISearchControllerDelegate, CLLocationManagerDelegate>

// search
@property(nonatomic, strong) UISearchController *searchController;
@property(nonatomic, strong) NMASearchResultViewController *resultTableViewController;

// waypoints
@property(nonatomic, strong) NSArray *waypointsPlaces; // Array of NMAPlaces

@property(nonatomic, strong) NMAPlacesClient *placesClient;

@property(nonatomic, strong) CLLocation *currentLocation;

@property(nonatomic, strong) NMARoute *route;
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) NMAEmptyTableHandler *emptyTableHandler;
@end

@implementation NMAWaypointsViewController {

}

#pragma mark - View Lifecylce

- (void)viewDidLoad
{
    [super viewDidLoad];

    // controller displaying the search result
    self.resultTableViewController = [[NMASearchResultViewController alloc] init];
    self.resultTableViewController.resultDelegate = self;

    // Setup search bar
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultTableViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.showsScopeBar = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;


    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = YES;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchBar.placeholder = NSLocalizedString(@"Add destination", nil);

    self.definesPresentationContext = YES;

    // table cells
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([NMAPlaceTableViewCell class]) bundle:nil] forCellReuseIdentifier:NMAWaypointsViewControllerCellId];

    // table view

    self.tableView.estimatedRowHeight = 70.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    // handling empty tables
    self.emptyTableHandler = [NMAEmptyTableHandler handlerWithTitle:NSLocalizedString(@"Plan your journey", nil)
                                                           subtitle:NSLocalizedString(@"Add a one or more destinations and can re-order them.", nil)
                                                              image:[UIImage imageNamed:@"context_menu_maps"]
                                              emptyTableTappedBlock:^{
                                                  [self.searchController.searchBar becomeFirstResponder];
                                              }];

    self.tableView.emptyDataSetSource = self.emptyTableHandler;
    self.tableView.emptyDataSetDelegate = self.emptyTableHandler;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // model

    self.placesClient = [[NMAPlacesClient alloc] init];
    self.waypointsPlaces = [NSArray array];


}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // todo, ask nicely
    [self.locationManager mna_requestAuthorizationIfNeeded];
    [self.locationManager startUpdatingLocation];
}


#pragma mark - Actions

- (IBAction)routeButtonAction:(id)sender
{
    if (self.waypointsPlaces.count < 1) {
        //todo inform user if current location not available
        return;
    }
    [self performSegueWithIdentifier:NMAShowRouteSelectionSegueId sender:self];

}

- (IBAction)editButtonAction:(id)sender
{
    self.tableView.editing = !self.tableView.editing;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];

    if ([segue.identifier isEqualToString:NMAShowRouteSelectionSegueId]) {
        NMARouteSelectionViewController *routeSelectionViewController = segue.destinationViewController;

        // add the current location as first place
        if (self.currentLocation) {
            NSMutableArray *waypoints = [self.waypointsPlaces mutableCopy];
            [waypoints insertObject:[NMAPlace placeWithPosition:self.currentLocation] atIndex:0];
            routeSelectionViewController.waypointsPlaces = waypoints;
        }
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.waypointsPlaces.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NMAWaypointsViewControllerCellId forIndexPath:indexPath];

    [self nma_configurePlaceCell:cell withPlace:self.waypointsPlaces[(NSUInteger) indexPath.row]];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSMutableArray *mutablePlaces = [self.waypointsPlaces mutableCopy];
    id itemToMove = mutablePlaces[(NSUInteger) sourceIndexPath.row];
    [mutablePlaces removeObjectAtIndex:(NSUInteger) sourceIndexPath.row];
    [mutablePlaces insertObject:itemToMove atIndex:(NSUInteger) destinationIndexPath.row];

    self.waypointsPlaces = [mutablePlaces copy];

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray *mutablePlaces = [self.waypointsPlaces mutableCopy];
        [mutablePlaces removeObjectAtIndex:(NSUInteger) indexPath.row];
        self.waypointsPlaces = [mutablePlaces copy];
        [self.tableView reloadData];
    }
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{

    NSString *query = searchController.searchBar.text;

    self.resultTableViewController.searchText = query;

    if (query.length < 1) {
        return;
    }

    self.resultTableViewController.loading = YES;
    [self.placesClient searchPlaces:query atLocation:self.currentLocation.coordinate completion:^(NSArray *places, NSError *error) {
        self.resultTableViewController.loading = NO;
        self.resultTableViewController.searchError = error;
        self.resultTableViewController.placesArray = places;

    }];

}

#pragma mark - UISearchControllerDelegate

- (void)didDismissSearchController:(UISearchController *)searchController
{
    [self clearSearchTable];
}


#pragma mark - TASearchResultTableViewControllerDelegate

- (void)searchResultTableViewController:(NMASearchResultViewController *)controller didSelectPlace:(NMAPlace *)place
{
    NSLog(@"Adding waypoint %@", place);
    NSMutableArray *mutablePlaces = [self.waypointsPlaces mutableCopy];
    [mutablePlaces addObject:place];

    self.waypointsPlaces = [mutablePlaces copy];

    [self clearSearchTable];
    [self.tableView reloadData];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.currentLocation = [locations firstObject];

}

#pragma mark - Private

- (void)clearSearchTable
{
    self.searchController.active = NO;
    self.resultTableViewController.placesArray = nil;
}

- (void)setWaypointsPlaces:(NSArray *)waypointsPlaces
{
    _waypointsPlaces = waypointsPlaces;
    self.searchController.searchBar.placeholder = waypointsPlaces.count == 0 ? NSLocalizedString(@"Add destination", nil) : NSLocalizedString(@"Add a stop-over", nil);
}


@end