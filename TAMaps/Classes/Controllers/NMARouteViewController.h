//
//  NMARouteViewController.h
//  TAMaps
//
//  Created by Jan Chaloupecky on 07/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@import NMAKit;

@interface NMARouteViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NMARoute *route;

@end

