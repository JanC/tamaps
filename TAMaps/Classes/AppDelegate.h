//
//  AppDelegate.h
//  TAMaps
//
//  Created by Jan Chaloupecky on 07/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

