//
// Created by Jan on 12/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMARoute.h"

@interface UIImage (NMAImage)

+(UIImage *) nma_imageForTransportMode:(NMARoutingTransportMode) transportMode;
@end