//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (NMAFormat)

- (NSString *)nma_formatDurationValue;

- (NSString *)nma_formatDistanceValue;
@end