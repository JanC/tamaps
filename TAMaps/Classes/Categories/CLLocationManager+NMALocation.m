//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "CLLocationManager+NMALocation.h"


@implementation CLLocationManager (NMALocation)

+ (BOOL)locationServicesEnabledAndAuthorized
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    return ([CLLocationManager locationServicesEnabled] &&
            ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ||
                    [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways));
#else
    return ([CLLocationManager locationServicesEnabled] &&
            [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized);

#endif
}

- (void)mna_requestAuthorizationIfNeeded
{
    if(![CLLocationManager locationServicesEnabledAndAuthorized]) {
        [self requestWhenInUseAuthorization];
    }
}
@end