//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NSNumber+NMAFormat.h"

@import CoreLocation;


@implementation NSNumber (NMAFormat)


- (NSString *)nma_formatDistanceValue
{

    static NSNumberFormatter *distanceFormatter;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        distanceFormatter = [[NSNumberFormatter alloc] init];
        [distanceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [distanceFormatter setMaximumFractionDigits:2];
        [distanceFormatter setDecimalSeparator:@","];
    });

    CLLocationDistance distance = [self doubleValue];
    NSString *units;
    if (distance > 1000.0) {
        distance /= 1000.0;
        units = @"km";
    } else {
        distance = round(distance);
        units = @"m";
    }
    NSString *value = [NSString stringWithFormat:@"%@ %@", [distanceFormatter stringFromNumber:@(distance)], units];
    return value;


}

- (NSString *)nma_formatDurationValue
{
    NSString *result;
    NSInteger seconds = [self integerValue];
    NSInteger hours = seconds / (60 * 60);
    seconds -= hours * (60 * 60);
    NSInteger minutes = seconds / 60;

    result = [NSString stringWithFormat:@"%02ldh %02ldm", (long) hours, (long) minutes];

    if (hours > 24) {
        NSInteger days = hours / 24;
        hours = hours % 24;
        result = [NSString stringWithFormat:@"%ldd %02ldh %02ldm", (long) days, (long) hours, (long) minutes];

    }
    return result;

}
@end