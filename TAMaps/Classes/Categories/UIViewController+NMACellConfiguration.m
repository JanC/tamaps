//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "UIViewController+NMACellConfiguration.h"
#import "NMAPlaceTableViewCell.h"
#import "NMAPlace.h"
#import "NMARoute.h"
#import "NMASummary.h"
#import "NMARouteTableViewCell.h"
#import "UIImage+NMAImage.h"
#import "NMARoutingOptions.h"
#import "NSNumber+NMAFormat.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation UIViewController (NMACellConfiguration)

#pragma mark - Cell Configuration

- (UITableViewCell *)nma_configurePlaceCell:(UITableViewCell *)tableViewCell withPlace:(NMAPlace *)place
{
    NMAPlaceTableViewCell *cell = (NMAPlaceTableViewCell *) tableViewCell;

    cell.placeTitleLabel.text = place.title;
    cell.placeSubtitleLabel.text = place.vicinity;

    [cell.placeImageView sd_setImageWithURL:place.icon placeholderImage:[UIImage imageNamed:@"06"]];
    return cell;
}

- (UITableViewCell *)nma_configureRouteSummaryCell:(UITableViewCell *)tableViewCell withRoute:(NMARoute *)route transportMode:(NMARoutingTransportMode)transportMode
{
    NMARouteTableViewCell *cell = (NMARouteTableViewCell *) tableViewCell;
    cell.distanceLabel.text = NSLocalizedString(@"Not available", nil);
    cell.travelTimeLabel.text = @" ";
    if (route) {
        cell.distanceLabel.text = [route.summary.distance nma_formatDistanceValue];
        cell.travelTimeLabel.text = [route.summary.travelTime nma_formatDurationValue];
    }

    cell.transportModeImageView.image = [UIImage nma_imageForTransportMode:transportMode];
    return cell;
}

@end