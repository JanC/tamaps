//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "MKMapView+TAMapView.h"


@implementation MKMapView (TAMapView)

// returns a MKCoordinateRegion that encompasses an array of MKAnnotations

+ (MKCoordinateRegion)ta_regionForAnnotations:(NSArray *)annotations
{

    CLLocationDegrees minLat = 90.0;
    CLLocationDegrees maxLat = -90.0;
    CLLocationDegrees minLon = 180.0;
    CLLocationDegrees maxLon = -180.0;

    for (id <MKAnnotation> annotation in annotations) {
        if (annotation.coordinate.latitude < minLat) {
            minLat = annotation.coordinate.latitude;
        }
        if (annotation.coordinate.longitude < minLon) {
            minLon = annotation.coordinate.longitude;
        }
        if (annotation.coordinate.latitude > maxLat) {
            maxLat = annotation.coordinate.latitude;
        }
        if (annotation.coordinate.longitude > maxLon) {
            maxLon = annotation.coordinate.longitude;
        }
    }

    MKCoordinateSpan span = MKCoordinateSpanMake(maxLat - minLat, maxLon - minLon);

    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((maxLat - span.latitudeDelta / 2), maxLon - span.longitudeDelta / 2);

    return MKCoordinateRegionMake(center, span);
}
@end