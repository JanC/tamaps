//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;

@interface CLLocationManager (NMALocation)


- (void)mna_requestAuthorizationIfNeeded;

@end