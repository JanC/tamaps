//
// Created by Jan on 12/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "UIImage+NMAImage.h"


@implementation UIImage (NMAImage)

+(UIImage *) nma_imageForTransportMode:(NMARoutingTransportMode) transportMode
{
    UIImage *image = [UIImage imageNamed:@"icon_directions_drive_inactive"];

    switch(transportMode) {

        case NMARoutingTransportModeCar:
            image = [UIImage imageNamed:@"icon_directions_drive_inactive"];
            break;
        case NMARoutingTransportModePedestrian:
            image = [UIImage imageNamed:@"icon_directions_walk_inactive"];
            break;
        case NMARoutingTransportModePublicTransport:
            image = [UIImage imageNamed:@"icon_directions_public_transport_inactive"];
            break;
    }

    return image;
}
@end