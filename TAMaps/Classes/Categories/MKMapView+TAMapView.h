//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

@interface MKMapView (TAMapView)

+ (MKCoordinateRegion)ta_regionForAnnotations:(NSArray *)annotations;
@end