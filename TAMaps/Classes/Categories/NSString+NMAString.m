//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NSString+NMAString.h"


@implementation NSString (NMAString)

- (NSString *)nma_replaceNewLines
{
    NSArray *split = [self componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    split = [split filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
    return [split componentsJoinedByString:@" "];
}

@end