//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMARoute.h"

@class NMAPlace;
@class NMARoute;

@interface UIViewController (NMACellConfiguration)

- (UITableViewCell *)nma_configurePlaceCell:(UITableViewCell *)tableViewCell withPlace:(NMAPlace *)place;

- (UITableViewCell *)nma_configureRouteSummaryCell:(UITableViewCell *)tableViewCell withRoute:(NMARoute *)route transportMode:(NMARoutingTransportMode)transportMode;
@end