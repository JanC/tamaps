//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NMASpecHelper : NSObject

#pragma mark - Places API

+ (void)enablePlaceSearchStubValid;
+ (void)enablePlaceSearchStubNetworkError;
+ (void)enablePlaceSearchStubInvalidJSON;
+ (void)enablePlaceSearchStubCorruptJSON;

#pragma mark - Route API

+ (void)enableRouteCalculateStubValid;
+ (void)enableRouteCalculateStubNetworkError;

+ (void)disableStubs;
@end