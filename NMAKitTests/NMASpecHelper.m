//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMASpecHelper.h"
#import <OHHTTPStubs/OHHTTPStubs.h>


@implementation NMASpecHelper {

}

+ (void)enablePlaceSearchStubValid
{
    [self enableStubForURL:@"places.hybrid.api.here.com" withFileName:@"place-search-response-valid.json"];
}

+ (void)enablePlaceSearchStubNetworkError
{
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.host isEqualToString:@"places.hybrid.api.here.com"];
    }                   withStubResponse:^OHHTTPStubsResponse *(NSURLRequest *request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSError *error = [NSError errorWithDomain:@"foo.bar" code:500 userInfo:nil];
        return [OHHTTPStubsResponse responseWithError:error];
    }];

}

+ (void)enablePlaceSearchStubInvalidJSON
{
    [self enableStubForURL:@"places.hybrid.api.here.com" withFileName:@"place-search-response-invalid.json"];

}

+ (void)enablePlaceSearchStubCorruptJSON
{
    [self enableStubForURL:@"places.hybrid.api.here.com" withFileName:@"place-search-response-corrupt.json"];
}

#pragma mark - Route API

+ (void)enableRouteCalculateStubValid
{
    [self enableStubForURL:@"route.cit.api.here.com" withFileName:@"route-calculate-response-valid.json"];
}

+ (void)enableRouteCalculateStubNetworkError
{
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.host isEqualToString:@"route.cit.api.here.com"];
    }                   withStubResponse:^OHHTTPStubsResponse *(NSURLRequest *request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSError *error = [NSError errorWithDomain:@"foo.bar" code:500 userInfo:nil];
        return [OHHTTPStubsResponse responseWithError:error];
    }];
}

+ (void)disableStubs
{
    [OHHTTPStubs removeAllStubs];
}

#pragma mark - Private

+ (void)enableStubForURL:(NSString *)url withFileName:(NSString *)fileName
{
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.host isEqualToString:url];
    }                   withStubResponse:^OHHTTPStubsResponse *(NSURLRequest *request) {
        // Stub it with our "wsresponse.json" stub file (which is in same bundle as self)
        NSString *fixture = OHPathForFileInBundle(fileName, nil);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture
                                                statusCode:200 headers:@{ @"Content-Type" : @"application/json" }];
    }];
}
@end