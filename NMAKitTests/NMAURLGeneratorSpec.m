//
//  NMAURLGeneratorSpec.m
//  TAMaps
//
//  Created by Jan on 09/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import <UIKit/UIKit.h>
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>

#import "NMASpecHelper.h"
#import "NMAPlacesClient.h"
#import "NMAPlace.h"
#import "NMAURLGenerator.h"


SpecBegin(NMAURLGenerator)
    describe(@"NMAURLGeneratorSpec", ^{

        it(@"should generate search url with parameter values", ^{
            NMAURLGenerator *sut = [[NMAURLGenerator alloc] initWithBaseURL:@"http://foo.com" path:@"places/v1/discover/search" baseParameters:nil];

            NSURL *url = [sut URLWithParameters:@{
                    @"param1" : @"value1",
                    @"param2" : @"value 2",
            }];

            expect(url.absoluteString).to.equal(@"http://foo.com/places/v1/discover/search?param1=value1&param2=value%202");
        });

        it(@"should generate search url with base parameters values", ^{
            NMAURLGenerator *sut = [[NMAURLGenerator alloc] initWithBaseURL:@"http://foo.com" path:@"places/v1/discover/search" baseParameters: @{@"app_id" : @"myAppId"}];

            NSURL *url = [sut URLWithParameters:@{
                    @"param1" : @"value1",
                    @"param2" : @"value 2",
            }];

            expect(url.absoluteString).to.equal(@"http://foo.com/places/v1/discover/search?app_id=myAppId&param1=value1&param2=value%202");
        });
    });
SpecEnd