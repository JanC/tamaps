//
//  NMAPlacesClientSpec.m
//  TAMaps
//
//  Created by Jan on 09/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <Mantle/MTLModel.h>

#import "NMASpecHelper.h"
#import "NMAPlacesClient.h"
#import "NMAPlace.h"


SpecBegin(NMAPlacesClient)

    describe(@"NMAPlacesClient", ^{

        __block NMAPlacesClient *sut;

        beforeEach(^{
            sut = [[NMAPlacesClient alloc] init];
        });

        afterEach(^{
            sut = nil;
           // [NMASpecHelper disableStubs];
        });

        it(@"should return a list of parsed places", ^{

            [NMASpecHelper enablePlaceSearchStubValid];

            waitUntil(^(DoneCallback done) {

                CLLocationCoordinate2D location = CLLocationCoordinate2DMake(52.544071, 13.419904);
                [sut searchPlaces:@"Schliemann" atLocation:location completion:^(NSArray *places, NSError *error) {

                    expect(places).to.beKindOf([NSArray class]);

                    id place = [places firstObject];

                    expect(place).to.beKindOf([NMAPlace class]);

                    done();
                }];
            });


        });

        it(@"should return a an error when no location is supplied ", ^{
            [NMASpecHelper enablePlaceSearchStubInvalidJSON];

            waitUntil(^(DoneCallback done) {
                [sut searchPlaces:@"foo" atLocation:kCLLocationCoordinate2DInvalid completion:^(NSArray *places, NSError *error) {
                    expect(error).toNot.beNil();
                    done();
                }];
            });

        });

        it(@"should return a error when invalid json is received", ^{
            [NMASpecHelper enablePlaceSearchStubInvalidJSON];

            waitUntil(^(DoneCallback done) {
                CLLocationCoordinate2D location = CLLocationCoordinate2DMake(52.544071, 13.419904);
                [sut searchPlaces:@"foo" atLocation:location completion:^(NSArray *places, NSError *error) {
                    expect(error).toNot.beNil();
                    done();
                }];
            });
        });

        it(@"should return a error a HTTP error code is received", ^{
            [NMASpecHelper enablePlaceSearchStubNetworkError];

            waitUntil(^(DoneCallback done) {
                CLLocationCoordinate2D location = CLLocationCoordinate2DMake(52.544071, 13.419904);
                [sut searchPlaces:@"foo" atLocation:location completion:^(NSArray *places, NSError *error) {
                    expect(error).toNot.beNil();
                    done();
                }];
            });
        });

    });
SpecEnd