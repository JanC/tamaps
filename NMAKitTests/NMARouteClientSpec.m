//
//  TAHereAPIClientSpec.m
//  TAMaps
//
//  Created by Jan Chaloupecky on 07/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import "NMARouteClient.h"
#import "NMAManeuver.h"
#import "NMASpecHelper.h"
#import "NMARoute.h"


SpecBegin(NMARouteClient)
    describe(@"NMARouteClient", ^{

        __block NMARouteClient *sut;
        beforeEach(^{
            sut = [[NMARouteClient alloc] init];
        });

        afterEach(^{
            sut = nil;
            [NMASpecHelper disableStubs];
        });

        it(@"should get a valid route meneuvres", ^{
            waitUntil(^(DoneCallback done) {

                [NMASpecHelper enableRouteCalculateStubValid];

                CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:52.5441561 longitude:13.419845];
                CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:52.5305129 longitude:13.3840783];
                [sut calculateRouteWithStartLocation:startLocation options:nil waypoints:@[ endLocation ] completion:^(NMARoute *route, NSError *error) {

                    expect(route).to.beKindOf([NMARoute class]);

                    done();
                }];
            });
        });

        it(@"should fail with error", ^{
            waitUntil(^(DoneCallback done) {

                [NMASpecHelper enableRouteCalculateStubNetworkError];

                CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:52.5441561 longitude:13.419845];
                CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:52.5305129 longitude:13.3840783];
                [sut calculateRouteWithStartLocation:startLocation options:nil waypoints:@[ endLocation ] completion:^(NMARoute *route, NSError *error) {

                    expect(error).toNot.beNil();
                    done();
                }];
            });
        });
    });
SpecEnd
