//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//


#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OHHTTPStubs/OHHTTPStubsResponse.h>
#import "MTLModel.h"
#import <Mantle/MTLJSONAdapter.h>
#import "NMAJSONPlacesResponseParser.h"
#import "NMAPlace.h"
#import "NMAManeuver.h"
#import "NMAJSONRouteResponseParser.h"
#import "NMARoute.h"
#import "NMASummary.h"
#import "NMALeg.h"

SpecBegin(NMAJSONRouteResponseParser)
    describe(@"NMAJSONRouteResponseParser", ^{

        NMAJSONRouteResponseParser *sut = [[NMAJSONRouteResponseParser alloc] init];

        it(@"should parse a valid json route", ^{

        });

        it(@"should parse a valid route summary ", ^{
            NSData *data = [NSData dataWithContentsOfFile:OHPathForFileInBundle(@"route-calculate-response-valid.json", nil)];
            NSError *error;
            NMARoute *route = [sut parseCalculateRouteResponseData:data error:&error];
            expect(error).to.beNil();

            expect(route).toNot.beNil();
            expect(route.summary).toNot.beNil();

            expect(route.summary.text).to.equal(@"The trip takes 2.6 km and 6 mins.");
            expect(route.summary.distance).to.equal(2592);
            expect(route.summary.trafficTime).to.equal(368);
            expect(route.summary.travelTime).to.equal(339);



        });

        it(@"should parse a valid route legs with maneuvers", ^{
            NSData *data = [NSData dataWithContentsOfFile:OHPathForFileInBundle(@"route-calculate-response-valid.json", nil)];
            NSError *error;
            NMARoute *route = [sut parseCalculateRouteResponseData:data error:&error];
            expect(error).to.beNil();

            NSArray *legs = route.legs;
            expect(legs.count).to.equal(1); // should have two legs and walk :)

            NMALeg *leg = [legs firstObject];

            expect(leg).to.beKindOf([NMALeg class]);

            expect(leg.maneuvers.count).to.equal(5);

            NMAManeuver *maneuver = [leg.maneuvers firstObject];

            expect(maneuver.instruction).to.equal(@"Head toward Stargarder Straße on Schliemannstraße. Go for 151 m.");
            expect(maneuver.position.coordinate.latitude).to.equal(52.5440625);
            expect(maneuver.position.coordinate.longitude).to.equal(13.4200832);
        });

        it(@"should return an error with invalid json", ^{
            NSData *data = [NSData dataWithContentsOfFile:OHPathForFileInBundle(@"place-search-response-invalid.json", nil)];
            NSError *error;
            NMARoute *route = [sut parseCalculateRouteResponseData:data error:&error];
            expect(error).toNot.beNil();
            expect(route).to.beNil();
        });

        it(@"should return an error with corrupt json", ^{
            NSData *data = [NSData dataWithContentsOfFile:OHPathForFileInBundle(@"place-search-response-corrupt.json", nil)];
            NSError *error;
            NMARoute *route= [sut parseCalculateRouteResponseData:data error:&error];
            expect(route).to.beNil();
        });

    });
SpecEnd