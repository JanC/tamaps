//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//


#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OHHTTPStubs/OHHTTPStubsResponse.h>
#import <Mantle/MTLModel.h>
#import "NMAJSONPlacesResponseParser.h"
#import "NMAPlace.h"

SpecBegin(NMAJSONPlacesResponseParser)
    describe(@"NMAJSONPlacesResponseParser", ^{

        NMAJSONPlacesResponseParser *sut = [[NMAJSONPlacesResponseParser alloc] init];

        it(@"should parse a valid json", ^{
            NSData *data = [NSData dataWithContentsOfFile:OHPathForFileInBundle(@"place-search-response-valid.json", nil)];
            NSError *error;
            NSArray *places = [sut parseSearchResponseData:data error:&error];
            expect(error).to.beNil();
            expect(places).toNot.beNil();

            // expect(places).toNot.beKindOf([NSArray class]); hmm this fails don't know why
            expect(places.count).to.equal(2);

            NMAPlace *place = places.firstObject;

            expect(place.title).to.equal(@"Schliemann");
            expect(place.vicinity).to.equal(@"Schliemannstr. 21 10437 Berlin");
            expect(place.position.coordinate.latitude).to.equal(52.54443);
            expect(place.position.coordinate.longitude).to.equal(13.42071);
        });

        it(@"should return an error with invalid json", ^{
            NSData *data = [NSData dataWithContentsOfFile:OHPathForFileInBundle(@"place-search-response-invalid.json", nil)];
            NSError *error;
            NSArray *places = [sut parseSearchResponseData:data error:&error];
            expect(error).toNot.beNil();
            expect(places).to.beNil();
        });

        it(@"should return an error with corrupt json", ^{
            NSData *data = [NSData dataWithContentsOfFile:OHPathForFileInBundle(@"place-search-response-corrupt.json", nil)];
            NSError *error;
            NSArray *places = [sut parseSearchResponseData:data error:&error];
            expect(places).to.beNil();
        });

    });
SpecEnd