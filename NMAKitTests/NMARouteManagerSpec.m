//
//  NMARouteManagerSpec.m
//  TAMaps
//
//  Created by Jan on 11/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Specta/Specta.h>
#import "MTLModel.h"
#import "NMARouteManager.h"
#import "NMASpecHelper.h"
#import "OCMock.h"
#import "NMARoutingOptions.h"

CGFloat static NMARouteManagerSpecTimeOut = 10;

SpecBegin(NMARouteManager)
    describe(@"NMARouteManager", ^{
        __block NMARouteManager *sut;
        __block id protocolMock;

        beforeEach(^{
            sut = [[NMARouteManager alloc] initWithWaypoints:@[ ]];
            protocolMock = OCMProtocolMock(@protocol(NMARouteManagerDelegate));
            sut.delegate = protocolMock;
        });

        afterEach(^{
            sut = nil;
            protocolMock = nil;
            [NMASpecHelper disableStubs];
        });

        it(@"should call calculated route delegate with correct transport mode", ^{
            [NMASpecHelper enableRouteCalculateStubValid];

            NMARoutingOptions *options = [NMARoutingOptions optionsWithRoutingTransportMode:NMARoutingTransportModeCar routingType:NMARoutingTypeFastest];

            OCMExpect([protocolMock routeManager:sut calculatedRoute:[OCMArg checkWithBlock:^BOOL(id obj) {
                return obj == nil;
            }]]);

            [sut calculateRoutesWithOptions:options];


            OCMVerifyAllWithDelay(protocolMock, NMARouteManagerSpecTimeOut);

        });

        it(@"should call error route delegate with correct transport mode", ^{
            [NMASpecHelper enableRouteCalculateStubNetworkError];

            NMARoutingOptions *options = [NMARoutingOptions optionsWithRoutingTransportMode:NMARoutingTransportModeCar routingType:NMARoutingTypeFastest];

            [sut calculateRoutesWithOptions:options];


            OCMExpect([protocolMock routeManager:sut failedWithError:[OCMArg any] forTransportMode:NMARoutingTransportModeCar]);

            OCMVerifyAllWithDelay(protocolMock, NMARouteManagerSpecTimeOut);

        });

        it(@"should call skipped route delegate with correct transport mode", ^{
            [NMASpecHelper enableRouteCalculateStubValid];

            NMARoutingOptions *options = [NMARoutingOptions optionsWithRoutingTransportMode:NMARoutingTransportModeCar routingType:NMARoutingTypeFastest];

            [sut calculateRoutesWithOptions:options];

            OCMVerify([protocolMock routeManager:sut calculatedRoute:[OCMArg isNil]]);

        });

    });
SpecEnd


