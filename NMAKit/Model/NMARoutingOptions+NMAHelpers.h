//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMARoutingOptions.h"

@interface NMARoutingOptions (NMAHelpers)
@property (nonatomic, assign, readonly) NSString * routingTransportModeString;
@property (nonatomic, assign, readonly) NSString * routingTypeString;
@end