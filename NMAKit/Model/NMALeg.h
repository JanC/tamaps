//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NMALeg : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSArray *maneuvers; // NMAMAneuver

@property (nonatomic, strong) NSNumber *length;
@property (nonatomic, strong) NSNumber *travelTime;

@property (nonatomic, strong) NSString *startName;
@property (nonatomic, strong) NSString *endName;

@end