//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMASummary.h"


@implementation NMASummary {

}


#pragma mark - Mantle

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"distance": @"distance",
            @"trafficTime": @"trafficTime",
            @"travelTime": @"travelTime",
            @"text": @"text",
    };
}

@end