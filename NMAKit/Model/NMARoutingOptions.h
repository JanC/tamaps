//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMARoute.h"


@interface NMARoutingOptions : NSObject

@property (nonatomic, assign) NMARoutingType routingType;
@property (nonatomic, assign) NMARoutingTransportMode routingTransportMode;

- (instancetype)initWithRoutingTransportMode:(NMARoutingTransportMode)routingTransportMode routingType:(NMARoutingType)routingType;

+ (instancetype)optionsWithRoutingTransportMode:(NMARoutingTransportMode)routingTransportMode routingType:(NMARoutingType)routingType;

+(instancetype) defaultOptions;


@end