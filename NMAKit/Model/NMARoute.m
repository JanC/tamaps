//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARoute.h"
#import "NMASummary.h"
#import "NMALeg.h"
#import "NMARoutingOptions.h"


@implementation NMARoute {

}

- (NSArray *)allManeuvres
{
    NSMutableArray *manevres = [self.legs valueForKeyPath:@"@unionOfArrays.maneuvers"];
    return manevres;
}

#pragma mark - Mantle

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"summary" : @"summary",
            @"legs" : @"leg",
    };
}


+ (NSValueTransformer *)summaryJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[NMASummary class]];

}

+ (NSValueTransformer *)legsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[NMALeg class]];
}


@end