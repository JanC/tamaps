//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARoutingOptions.h"


@implementation NMARoutingOptions {

}
- (instancetype)initWithRoutingTransportMode:(NMARoutingTransportMode)routingTransportMode routingType:(NMARoutingType)routingType
{
    self = [super init];
    if (self) {
        self.routingTransportMode = routingTransportMode;
        self.routingType = routingType;
    }

    return self;
}

+ (instancetype)optionsWithRoutingTransportMode:(NMARoutingTransportMode)routingTransportMode routingType:(NMARoutingType)routingType
{
    return [[self alloc] initWithRoutingTransportMode:routingTransportMode routingType:routingType];
}

+ (instancetype)defaultOptions
{
    return [[self alloc] initWithRoutingTransportMode:NMARoutingTransportModePedestrian | NMARoutingTransportModeCar | NMARoutingTransportModePublicTransport routingType:NMARoutingTypeFastest];
}

@end