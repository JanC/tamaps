//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Mantle/MTLValueTransformer.h>
#import "NMAPlace.h"
#import "NSString+NMAString.h"


@interface NMAPlace ()
@property(nonatomic, strong, readwrite) CLLocation *position;
@property(nonatomic, strong, readwrite) NSString *title;
@property(nonatomic, strong, readwrite) NSString *vicinity;
@end

@implementation NMAPlace {

}

- (instancetype)initWithPosition:(CLLocation *)position
{
    self = [super init];
    if (self) {
        self.position = position;
    }

    return
            self;
}

+ (instancetype)placeWithPosition:(CLLocation *)position
{
    return [[self alloc] initWithPosition:position];
}

#pragma mark - Mantle


+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"title" : @"title",
            @"vicinity" : @"vicinity",
            @"position" : @"position",
            @"icon" : @"icon",
    };
}

+ (NSValueTransformer *)vicinityJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *jsonString, BOOL *success, NSError **error) {
        return [jsonString nma_replaceNewLines];
    }];
}

+ (NSValueTransformer *)iconJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)positionJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray *positionArray, BOOL *success, NSError **error) {

        CLLocation *position;

        if (positionArray.count == 2) {
            position = [[CLLocation alloc] initWithLatitude:[[positionArray firstObject] doubleValue] longitude:[[positionArray lastObject] doubleValue]];
        }

        return position;

    }];
}

- (NSString *)description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"%@", self.title];
    [description appendFormat:@", %@", self.position];
    [description appendString:@">"];
    return description;
}


@end