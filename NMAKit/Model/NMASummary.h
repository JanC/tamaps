//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NMASummary : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSNumber *trafficTime;
@property (nonatomic, strong) NSNumber *travelTime;
@property (nonatomic, strong) NSString *text;

@end