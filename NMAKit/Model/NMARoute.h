//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import <Mantle/MTLJSONAdapter.h>

@class NMASummary;
@class NMARoutingOptions;

typedef NS_ENUM(NSUInteger, NMARoutingType) {
    NMARoutingTypeFastest,
    NMARoutingTypeShortest
};

typedef NS_OPTIONS(NSUInteger, NMARoutingTransportMode) {
    NMARoutingTransportModeCar              = 1 << 0,
    NMARoutingTransportModePedestrian       = 1 << 1,
    NMARoutingTransportModePublicTransport  = 1 << 2,
};

@interface NMARoute : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NMASummary *summary;
@property (nonatomic, strong) NSArray *legs; // NMALeg

// Options used to calculate this route
@property (nonatomic, strong) NMARoutingOptions *options;



/**
* Returns the maneuvres of all legs in this route
*/
- (NSArray *)allManeuvres;

@end