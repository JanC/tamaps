//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/MTLJSONAdapter.h>
#import "MTLModel.h"

@import CoreLocation;



@interface NMAPlace : MTLModel <MTLJSONSerializing>

@property(nonatomic, strong, readonly) NSString *title;
@property(nonatomic, strong, readonly) NSString *vicinity;
@property(nonatomic, strong, readonly) CLLocation *position;
@property(nonatomic, strong, readonly) NSURL *icon;

- (instancetype)initWithPosition:(CLLocation *)position;

+ (instancetype)placeWithPosition:(CLLocation *)position;


@end