//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAManeuver.h"
#import "MTLValueTransformer.h"


@implementation NMAManeuver {

}


- (CLLocationCoordinate2D)coordinate
{
    return self.position.coordinate;
}

- (NSString *)title
{
    return self.instruction;
}

- (NSString *)subtitle
{
    return nil;
}

#pragma mark - Mantle

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"instruction" : @"instruction",
            @"position" : @"position",
    };
}

+ (NSValueTransformer *)positionJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSDictionary *locationDictionary, BOOL *success, NSError **error) {
        CLLocationDegrees latitude = [locationDictionary[@"latitude"] doubleValue];
        CLLocationDegrees longitude = [locationDictionary[@"longitude"] doubleValue];
        return [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    }];
}


@end