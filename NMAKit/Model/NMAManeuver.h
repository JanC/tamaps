//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@import CoreLocation;
@import MapKit;


@interface NMAManeuver : MTLModel <MKAnnotation, MTLJSONSerializing>

@property (nonatomic, strong) NSString *instruction;
@property (nonatomic, strong) CLLocation *position;

@end