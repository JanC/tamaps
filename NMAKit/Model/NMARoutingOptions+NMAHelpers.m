//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARoutingOptions+NMAHelpers.h"


@implementation NMARoutingOptions (NMAHelpers)

- (NSString *)routingTransportModeString
{
    NSString *result = @"car";
    switch (self.routingTransportMode) {
        case NMARoutingTransportModeCar:
            result = @"car";
            break;
        case NMARoutingTransportModePedestrian:
            result = @"pedestrian";
            break;
        case NMARoutingTransportModePublicTransport:
            result = @"publicTransport";
            break;
    }
    return result;
}

- (NSString *)routingTypeString
{
    NSString *result = @"fastest";

    switch(self.routingType) {

        case NMARoutingTypeFastest:
            result = @"fastest";
            break;
        case NMARoutingTypeShortest:
            result = @"shortest";
            break;
    }
    return result;
}


@end
