//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMALeg.h"
#import "NMAManeuver.h"
#import "MTLValueTransformer.h"


@implementation NMALeg {

}

#pragma mark - Mantle

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
            @"maneuvers": @"maneuver",
            @"length": @"length",
            @"travelTime": @"travelTime",
            @"startName": @"start",
            @"endName": @"end",
    };
}

+ (NSValueTransformer *)maneuversJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[NMAManeuver class]];
}


// The startName and endName are mapped just to a NSString in my model so we have to extract them from the json dictionary here

+ (NSValueTransformer *)startNameJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSDictionary *legLocationDict, BOOL *success, NSError **error) {
        //[jsonString nma_replaceNewLines]
        return legLocationDict[@"label"];
    }];
}

+ (NSValueTransformer *)endNameJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSDictionary *legLocationDict, BOOL *success, NSError **error) {
        //[jsonString nma_replaceNewLines]
        return legLocationDict[@"label"];
    }];
}

#pragma mark - Private



@end