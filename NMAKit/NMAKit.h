//
//  NMAKit.h
//  NMAKit
//
//  Created by Jan Chaloupecky on 14/06/15.
//  Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NMAKit.
FOUNDATION_EXPORT double NMAKitVersionNumber;

//! Project version string for NMAKit.
FOUNDATION_EXPORT const unsigned char NMAKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like
#import <NMAKit/NMAPlace.h>
#import <NMAKit/NMALeg.h>
#import <NMAKit/NMAManeuver.h>
#import <NMAKit/NMASummary.h>
#import <NMAKit/NMAPlacesClient.h>
#import <NMAKit/NMARouteClient.h>
#import <NMAKit/NMARouteManager.h>


