//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMARoute.h"

@class NMARouteManager;
@class NMARoute;
@class NMARoutingOptions;

@protocol NMARouteManagerDelegate <NSObject>

/**
*  Delegate method called when the NMARouteManager finished calculating a route for the specified transport mode. If the transport mode was not present in the routing options,
*  this method will be called immediately with a nil route
*
*  @param manager      The manager that requested the routes
*  @param route        The calculated route or nil if the transport mode was not requested
*/
- (void)routeManager:(NMARouteManager *)manager calculatedRoute:(NMARoute *)route;

/**
*  Delegate method called in case of routing error
*
*  @param manager          The manager that requested the routes
*  @param error            The error
*  @param transportMode    The transport mode for which the route was requested
*/
- (void)routeManager:(NMARouteManager *)manager failedWithError:(NSError *)error forTransportMode:(NMARoutingTransportMode)transportMode;

@end

/**
*  Helper class to calculate route with routing options
*/
@interface NMARouteManager : NSObject


@property(nonatomic, weak) id <NMARouteManagerDelegate> delegate;

- (instancetype)initWithWaypoints:(NSArray *)waypoints;

/**
*  Calculates the routes for the specified routing options
*
*  @param options The options object used for each routing request. The routingTransportMode can be used as a mask to calculate different routes
*/
- (void)calculateRoutesWithOptions:(NMARoutingOptions *)options;


@end