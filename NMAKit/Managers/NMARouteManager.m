//
// Created by Jan on 11/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARouteManager.h"
#import "NMARouteClient.h"
#import "NMARoute.h"
#import "NMARoutingOptions.h"


@interface NMARouteManager ()

@property(nonatomic, strong) NMARouteClient *routeClient;
@property(nonatomic, strong) NSArray *waypoints; // NMAPlaces

@end

@implementation NMARouteManager {

}
- (instancetype)initWithWaypoints:(NSArray *)waypoints
{
    self = [super init];
    if (self) {
        self.waypoints = waypoints;
        self.routeClient = [[NMARouteClient alloc] init];
    }

    return self;
}

- (void)calculateRoutesWithOptions:(NMARoutingOptions *)options
{

    for (NSNumber *transportMode in @[ @(NMARoutingTransportModeCar), @(NMARoutingTransportModePedestrian), @(NMARoutingTransportModePublicTransport) ]) {
        NMARoutingTransportMode currentTransportMode = (NMARoutingTransportMode) transportMode.unsignedIntegerValue;

        // routing for this mode not requested, call delegate and go to next
        if ((options.routingTransportMode & currentTransportMode) == 0) {
            if ([self.delegate respondsToSelector:@selector(routeManager:calculatedRoute:)]) {

                [self.delegate routeManager:self calculatedRoute:nil];
            }
            continue;

        }

        // request the route and call delegates
        [self.routeClient calculateRouteWithPlaces:self.waypoints
                                       routingType:options.routingType
                                     transportMode:currentTransportMode completion:^(NMARoute *route, NSError *error) {

                    if (error && [self.delegate respondsToSelector:@selector(routeManager:failedWithError:forTransportMode:)]) {
                        [self.delegate routeManager:self failedWithError:error forTransportMode:currentTransportMode];
                    }

                    if ([self.delegate respondsToSelector:@selector(routeManager:calculatedRoute:)]) {
                        [self.delegate routeManager:self calculatedRoute:route];
                    }

                }];
    }

}


@end