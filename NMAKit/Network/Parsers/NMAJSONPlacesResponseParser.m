//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAJSONPlacesResponseParser.h"
#import "NMAPlace.h"


@implementation NMAJSONPlacesResponseParser {

}
- (NSArray *)parseSearchResponseData:(NSData *)responseData error:(NSError **)error
{
    NSError *jsonError;
    NSDictionary *searchResponse = [self validateJSONData:responseData error:&jsonError];
    // json error
    if (jsonError) {
        *error = jsonError;
        return nil;
    }

    NSMutableArray *places = [NSMutableArray array];

    NSArray *items = searchResponse[@"results"][@"items"];

    [items enumerateObjectsUsingBlock:^(NSDictionary *placeDict, NSUInteger idx, BOOL *stop) {

        NSError *placeParseError;
        NMAPlace *place = [MTLJSONAdapter modelOfClass:NMAPlace.class fromJSONDictionary:placeDict error:&placeParseError];
        if(!placeParseError) {
            [places addObject:place];
        }

    }];


    return [places copy]; // non mutable
}

#pragma mark - Private


- (NSDictionary *)validateJSONData:(NSData *)data error:(NSError **)error
{

    NSDictionary *userInfo = @{
            NSLocalizedDescriptionKey : NSLocalizedString(@"Invalid response format.", nil),
    };

    NSError *jsonError;
    // json parsing
    NSDictionary *validJson;
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];


    if (!jsonError && ![json isKindOfClass:[NSDictionary class]]) {
        jsonError = [NSError errorWithDomain:@"com.here" code:0 userInfo:userInfo];
    }

    if (!jsonError && ![json[@"results"] isKindOfClass:[NSDictionary class]]) {
        jsonError = [NSError errorWithDomain:@"com.here" code:0 userInfo:userInfo];
    }

    if (!jsonError && ![json[@"results"][@"items"] isKindOfClass:[NSArray class]]) {
        jsonError = [NSError errorWithDomain:@"com.here" code:0 userInfo:userInfo];
    }

    if (jsonError) {
        *error = jsonError;
    }

    validJson = jsonError ? nil : json;

    return validJson;
}

@end