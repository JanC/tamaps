//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NMARouteResponseParser.h"


@interface NMAJSONRouteResponseParser : NSObject <NMARouteResponseParser>
@end