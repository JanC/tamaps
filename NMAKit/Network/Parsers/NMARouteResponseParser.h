//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NMARoute;

/**
 *  This protocol represents an abstract way of parsing thre API responses. HERE supports both JOSN and XML
 *  so it's up to the implementation to decide
 */
@protocol NMARouteResponseParser <NSObject>

@required
- (NMARoute *)parseCalculateRouteResponseData:(NSData *)responseData error:(NSError **)error;

@end