//
// Created by Jan on 10/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAJSONRouteResponseParser.h"
#import "NMAManeuver.h"
#import "NMARoute.h"
#import "NMASummary.h"


@implementation NMAJSONRouteResponseParser {

}
- (NMARoute *)parseCalculateRouteResponseData:(NSData *)responseData error:(NSError **)error

{


    NSError *jsonError;
    NSDictionary *routeResponse = [self validateJSONData:responseData error:&jsonError];
    // json error
    if (jsonError) {
        *error = jsonError;
        return nil;
    }

    // assume only one route :(
    NSDictionary *routeDict  = [routeResponse[@"response"][@"route"] firstObject];
    NMARoute *route = [MTLJSONAdapter modelOfClass:NMARoute.class fromJSONDictionary:routeDict error:&jsonError];


    return route;

}

#pragma mark - Private


- (NSDictionary *)validateJSONData:(NSData *)data error:(NSError **)error
{

    NSDictionary *userInfo = @{
            NSLocalizedDescriptionKey : NSLocalizedString(@"Invalid response format.", nil),
    };

    NSError *jsonError;
    // json parsing
    NSDictionary *validJson;
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];


    if (!jsonError && ![json isKindOfClass:[NSDictionary class]]) {
        jsonError = [NSError errorWithDomain:@"com.here" code:0 userInfo:userInfo];
    }

    if (!jsonError && ![json[@"response"] isKindOfClass:[NSDictionary class]]) {
        jsonError = [NSError errorWithDomain:@"com.here" code:0 userInfo:userInfo];
    }

    if (!jsonError && ![json[@"response"][@"route"] isKindOfClass:[NSArray class]]) {
        jsonError = [NSError errorWithDomain:@"com.here" code:0 userInfo:userInfo];
    }

    if (jsonError) {
        *error = jsonError;
    }

    validJson = jsonError ? nil : json;

    return validJson;
}

@end