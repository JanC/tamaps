//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NMAURLGenerator : NSObject
- (instancetype)initWithBaseURL:(NSString *)baseURL path:(NSString *)path baseParameters:(NSDictionary *)baseParameters;


/**
 *  Factory method for the Places API URL. The generator contains already non dynamic URL params needed
 *
 */
+(NMAURLGenerator*)URLGeneratorForPlacesAPI;


/**
*  Factory method for the Route API URL. The generator contains already non dynamic URL params needed
*
*/
+ (NMAURLGenerator *)URLGeneratorForRouteAPI;

/**
 *  <#Description#>
 *
 *  @param parameters <#parameters description#>
 *
 *  @return <#return value description#>
 */
-(NSURL *)URLWithParameters:(NSDictionary *)parameters;

-(NSURLRequest *) URLRequestWithParameters:(NSDictionary *)parameters;

@end