//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;

/**
*  Block executed when the search for places finishes
*
*  @param places An array of NMAPlaces
*  @param error  Error
*/
typedef void (^NMAPlacesSearchResponse)(NSArray *places, NSError *error);

@interface NMAPlacesClient : NSObject

/**
*  Sends a to the places search endpoint to lookup places.
*
*  @param query        The query to look for
*  @param location     The location to look around.
*  @param completion   The block executed after the request finishes.
*/
- (void)searchPlaces:(NSString *)query atLocation:(CLLocationCoordinate2D)location completion:(NMAPlacesSearchResponse)completion;

@end