//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;

#import "NMARoute.h"

@class NMAURLGenerator;
@class NMAJSONRouteResponseParser;
@class NMARoute;
@class NMARoutingOptions;

/**
*  Completion block of the route request
*
*  @param route Route object
*  @param error An Error :)
*/
typedef void (^TACalculateRouteCompletion)(NMARoute *route, NSError *error);

@interface NMARouteClient : NSObject


/**
*  Sends a to the "calculate route" endpoint with a list od CLLocations
*
*  @param startLocation The location where the route starts.
*  @param options       Routing options or nil to use default (car and fastest) See https://developer.here.com/rest-apis/documentation/routing/topics/resource-param-type-routing-mode.html
*  @param waypoints     A ordered list of CLLocation waypoints to go through.
*  @param completion    The block executed after the request finishes.
*/
- (void)calculateRouteWithStartLocation:(CLLocation *)startLocation
                                options:(NMARoutingOptions *)options
                              waypoints:(NSArray *)waypoints
                             completion:(TACalculateRouteCompletion)completion;


/**
*  Sends a request to the "calculate route" endpoint with a list od NMAPlaces
*
*  @param places        A array of NMAPlaces to go through including the start and end waypoints
*  @param options       Routing options or nil to use default (car and fastest) See https://developer.here.com/rest-apis/documentation/routing/topics/resource-param-type-routing-mode.html
*  @param waypoints     A ordered list of NMAPlaces waypoints to go through.
*  @param completion    The block executed after the request finishes.
*/

- (void)calculateRouteWithPlaces:(NSArray *)places
                     routingType:(NMARoutingType)routingType
                   transportMode:(NMARoutingTransportMode)transportMode
                      completion:(TACalculateRouteCompletion)completion;

@end