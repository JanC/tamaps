//
// Created by Jan Chaloupecky on 07/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMARouteClient.h"
#import "NMAManeuver.h"
#import "NMAURLGenerator.h"
#import "NMAJSONRouteResponseParser.h"
#import "NMAPlace.h"
#import "NMARoute.h"
#import "NMARoutingOptions.h"
#import "NMARoutingOptions+NMAHelpers.h"


@interface NMARouteClient ()
@property(nonatomic, strong) NMAURLGenerator *urlGenerator;
@property(nonatomic, strong) NMAJSONRouteResponseParser *parser;
@property(nonatomic, strong) NSURLSessionDataTask *task;
@end

@implementation NMARouteClient {

}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.urlGenerator = [NMAURLGenerator URLGeneratorForRouteAPI];
        self.parser = [[NMAJSONRouteResponseParser alloc] init];
    }

    return self;
}


- (void)calculateRouteWithPlaces:(NSArray *)places
                     routingType:(NMARoutingType)routingType
                   transportMode:(NMARoutingTransportMode)transportMode
                      completion:(TACalculateRouteCompletion)completion;
{
    NSMutableArray *mutableWaypoints = [NSMutableArray array];
    [places enumerateObjectsUsingBlock:^(NMAPlace *place, NSUInteger idx, BOOL *stop) {
        [mutableWaypoints addObject:place.position];
    }];

    NMARoutingOptions *options = [NMARoutingOptions optionsWithRoutingTransportMode:transportMode routingType:routingType];

    [self calculateRouteWithWaypoints:[mutableWaypoints copy] options:options completion:completion];
}

- (void)calculateRouteWithStartLocation:(CLLocation *)startLocation
                                options:(NMARoutingOptions *)options
                              waypoints:(NSArray *)waypoints
                             completion:(TACalculateRouteCompletion)completion
{
    NSMutableArray *mutableWaypoints = [waypoints mutableCopy];
    [mutableWaypoints insertObject:startLocation atIndex:0];
    [self calculateRouteWithWaypoints:[mutableWaypoints copy] options:options completion:completion];
}

- (void)calculateRouteWithWaypoints:(NSArray *)waypoints
                            options:(NMARoutingOptions *)options
                         completion:(TACalculateRouteCompletion)completion;
{
    if (!options) {
        options = [NMARoutingOptions optionsWithRoutingTransportMode:NMARoutingTransportModeCar routingType:NMARoutingTypeFastest];
    }

    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    [waypoints enumerateObjectsUsingBlock:^(CLLocation *location, NSUInteger idx, BOOL *stop) {
        // waypoint0=geo!52.5441561,13.419845
        NSString *key = [NSString stringWithFormat:@"waypoint%lu", (unsigned long) idx];
        NSString *value = [NSString stringWithFormat:@"geo!%.6f,%.6f", location.coordinate.latitude, location.coordinate.longitude];
        params[key] = value;
    }];

    // make as param

    params[@"mode"] = [NSString stringWithFormat:@"%@;%@;traffic:disabled", options.routingTypeString, options.routingTransportModeString];

    NSString *language = [[NSLocale preferredLanguages] firstObject];
    if (language) {
        params[@"language"] = language;
    }


    NSLog(@"route calculate with parameters %@", params);

    NSURLRequest *request = [self.urlGenerator URLRequestWithParameters:params];

    self.task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"response status code: %ld", (long) [httpResponse statusCode]);

        // network error
        if (error) {
            if (completion) {
                completion(nil, error);
            }
            return;
        }

        NSError *parseError;
        NMARoute *route = [self.parser parseCalculateRouteResponseData:data error:&parseError];
        route.options = options;
        NSLog(@"Found route %@ maneuvres", route);
        if (completion) {
            completion(route, parseError);
        }
    }];
    [self.task resume];

}

#pragma mark - Private


@end