//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAPlacesClient.h"
#import "NMAURLGenerator.h"
#import "NMAConstants.h"
#import "NMAPlacesResponseParser.h"
#import "NMAJSONPlacesResponseParser.h"


@interface NMAPlacesClient ()

@property(nonatomic, strong) NMAURLGenerator *urlGenerator;
@property(nonatomic, strong) id <NMAPlacesResponseParser> parser;

@property(nonatomic, strong) NSURLSessionTask *searchPlaceTask;
@end

@implementation NMAPlacesClient {

}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.urlGenerator = [NMAURLGenerator URLGeneratorForPlacesAPI];
        self.parser = [[NMAJSONPlacesResponseParser alloc] init];
    }

    return self;
}

#pragma mark - Public

- (void)searchPlaces:(NSString *)query atLocation:(CLLocationCoordinate2D)location completion:(NMAPlacesSearchResponse)completion
{

    [self.searchPlaceTask cancel]; // cancel previous search if any

    NSDictionary *params = @{
            NMAParamNameQ : query,
            NMAParamNameAT : [NSString stringWithFormat:@"%.6f,%.6f", location.latitude, location.longitude]
    };

    NSLog(@"Searching places with parameters %@", params);

    NSURLRequest *request = [self.urlGenerator URLRequestWithParameters:params];

    self.searchPlaceTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"response status code: %ld", (long) [httpResponse statusCode]);

        // network error
        if (error) {
            if (completion) {
                completion(nil, error);
            }
            return;
        }

        NSError *parseError;
        NSArray *places = [self.parser parseSearchResponseData:data error:&parseError];
        NSLog(@"Found %@ places", @(places.count));
        if (completion) {
            completion(places, parseError);
        }
    }];

    [self.searchPlaceTask resume];

}

#pragma mark - Private


@end