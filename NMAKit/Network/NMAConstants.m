//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAConstants.h"


@implementation NMAConstants

#pragma mark - Endpoints

NSString *const NMAPlacesClientBaseURL = @"https://places.hybrid.api.here.com";
NSString *const NMAPlacesClientSearchPath = @"places/v1/discover/search";

NSString *const NMARouteClientBaseURL = @"https://route.cit.api.here.com";
NSString *const NMARouteClientCalculatePath = @"routing/7.2/calculateroute.json";


#pragma mark - Request Parameters


NSString *const NMAParamNameAT = @"at";
NSString *const NMAParamNameQ = @"q";
NSString *const NMAParamNameAppId = @"app_id";
NSString *const NMAParamNameAppCode = @"app_code";
NSString *const NMAParamNameSize = @"size";
NSString *const NMAParamNameTF = @"tf";


@end