//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NMAConstants : NSObject

#pragma mark - Endpoints

FOUNDATION_EXPORT NSString *const NMAPlacesClientBaseURL;
FOUNDATION_EXPORT NSString *const NMAPlacesClientSearchPath;

FOUNDATION_EXPORT NSString *const NMARouteClientBaseURL;
FOUNDATION_EXPORT NSString *const NMARouteClientCalculatePath;


#pragma mark - Request Parameters


FOUNDATION_EXPORT NSString *const NMAParamNameAT;
FOUNDATION_EXPORT NSString *const NMAParamNameQ;
FOUNDATION_EXPORT NSString *const NMAParamNameAppId;
FOUNDATION_EXPORT NSString *const NMAParamNameAppCode;
FOUNDATION_EXPORT NSString *const NMAParamNameSize;
FOUNDATION_EXPORT NSString *const NMAParamNameTF;


@end