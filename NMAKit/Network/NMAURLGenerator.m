//
// Created by Jan on 09/06/15.
// Copyright (c) 2015 Tequila Apps. All rights reserved.
//

#import "NMAURLGenerator.h"
#import "NMAConstants.h"

NSString *const NMAAPIAppCode = @"yS6KV_zb6LDyF715lantvQ";

NSString *const NMAApiAppId = @"747a8ed841ad66fe713a5e36c564463b";

@interface NMAURLGenerator ()


@property(nonatomic, strong) NSString *path;
@property(nonatomic, strong) NSDictionary *baseParameters;

@end

@implementation NMAURLGenerator {

}
- (instancetype)initWithBaseURL:(NSString *)baseURL path:(NSString *)path baseParameters:(NSDictionary *)baseParameters
{
    self = [super init];
    if (self) {
        self.path = [NSString stringWithFormat:@"%@/%@", baseURL, path];
        self.baseParameters = baseParameters;
    }

    return self;
}

#pragma mark - Public

+ (NMAURLGenerator *)URLGeneratorForPlacesAPI
{
    NSDictionary *baseParams = @{
            NMAParamNameSize : @"20",
            NMAParamNameTF : @"plain",
            NMAParamNameAppCode : NMAAPIAppCode,
            NMAParamNameAppId : NMAApiAppId,
    };
    return [[NMAURLGenerator alloc] initWithBaseURL:NMAPlacesClientBaseURL path:NMAPlacesClientSearchPath baseParameters:baseParams];

}

+ (NMAURLGenerator *)URLGeneratorForRouteAPI
{
    NSDictionary *baseParams = @{
            NMAParamNameSize : @"20",
            NMAParamNameTF : @"plain",
            @"instructionFormat" : @"text",
            NMAParamNameAppCode : NMAAPIAppCode,
            NMAParamNameAppId : NMAApiAppId,
    };
    return [[NMAURLGenerator alloc] initWithBaseURL:NMARouteClientBaseURL path:NMARouteClientCalculatePath baseParameters:baseParams];

}


-(NSURLRequest *) URLRequestWithParameters:(NSDictionary *)parameters
{
    return [NSURLRequest requestWithURL:[self URLWithParameters:parameters]];
}

- (NSURL *)URLWithParameters:(NSDictionary *)parameters
{
    NSString *encodedParams = [self encodedStringFromParameters:parameters];
    NSString *urlString = [self.path stringByAppendingString:encodedParams];

    return [NSURL URLWithString:urlString];
}

#pragma mark - Private

- (NSString *)encodedStringFromParameters:(NSDictionary *)parameters
{
    NSMutableDictionary *mutableParameters = [parameters mutableCopy];
    [mutableParameters addEntriesFromDictionary:self.baseParameters];

    __block NSString *encodedParams = @"";
    NSArray *keys = [[mutableParameters allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];;
    [keys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
        NSString *value = mutableParameters[key];
        NSString *encodedString = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *currentKeyValue = [NSString stringWithFormat:@"%@%@=%@", idx == 0 ? @"?" : @"&", key, encodedString];
        encodedParams = [encodedParams stringByAppendingString:currentKeyValue];
    }];

    return encodedParams;
}

@end