![Image](https://www.bitrise.io/app/06c98394203ac3a8.svg?token=AVVEL7ZSVWM4uFl7crlAag)

# Nokia HERE challenge #

This is a sample app for Nokia here. 

### Get it running
```
git clone git@bitbucket.org:JanC/tamaps.git
cd tamaps
pod install
open TAMaps.xcworkspace
```

The `master` branch represents the state of the code after 4 evenings of work after receiving the challenge. The only change I did during the weekend was to create a `NMAKit` framework that containts all the model, network code.


### Unit Test & Code Coverage

To run the unit test + generate a code coverage report
```
gem install
rake ci:test
```

During my interview with Greg, I was told that HERE aimed a 80% of test code coverage so that's something I kept on mind.

![Image](./NMAKit/CodeCoverage.png)


## Classes decription

### Network Layer

`NMAPlacesClient` and `NMARouteClient` are the network layer classes interacting with the [Places](https://developer.here.com/rest-apis/documentation/places) and  [Routing](https://developer.here.com/rest-apis/documentation/routing) HERE API endpoints

### Parsing

The parsing of the API respones is not done by the clients directly but is encapsulated in the classes conforming to the `NMAPlacesResponseParser` and `NMARouteResponseParser` protocols used by the network clients. This takes away the responsibility of parsing from the clients and would allow to move the a XML API format only by implementing a new parser.

The parsing itself was first done manually but given the sort time I had for the challenge, I decided to use [Mantle](https://github.com/Mantle/Mantle) which accelerates grately the json parsing.


### Manager
The `NMARouteManager` uses the `NMARouteClient` to get all route types and delivers the results via delegation to the app. 

In fact, the manager should be an abstraction layer between the application and the network layer for all API requests. This would allow feature such as "recent searches", route cache and so on.


### Style
All the styling is done with the `NMAStyleKit` class. I borrowed the font, icons and colors from the official HERE ios app :)


### Libraries
I used the following 3rd party libraries

- [DZNEmptyDataSet](https://github.com/dzenbot/DZNEmptyDataSet) for empty table views. 
- [Mantle](https://github.com/Mantle/Mantle) for json parsing.
- [SDWebImage](https://github.com/rs/SDWebImage) to download the icons of places.
- [Specta](https://github.com/specta/specta), [Expecta](https://github.com/specta/expecta) for Unit tests.
- [OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs) to mock network responses to local files during unit tests/

### Localisation
All the UI messages of the app are localized but only in English. The HERE API uses the languge of the device so the route indications are localized with the languages supported by the API.

### CI

I used the [Bitrise](https://www.bitrise.io) service for continuous integration









